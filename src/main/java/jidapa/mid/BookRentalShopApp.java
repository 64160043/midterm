package jidapa.mid;

import java.util.Scanner;

public class BookRentalShopApp {

    static void print() {
        System.out.println("typebook");
        System.out.println("type1 = manga");
        System.out.println("type2 = magazine");
        System.out.println("type3 = programming");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BookRentalShop manga = new BookRentalShop("manga", 30, 5);
        BookRentalShop magazine = new BookRentalShop("magazine", 30, 5);
        BookRentalShop programming = new BookRentalShop("programming", 30, 5);

        print();
        System.out.print("Do you want to rent type : ");
        int type = sc.nextInt();
        if (type == 1) {
            System.out.print("Please enter your book : ");
            int book = sc.nextInt();

            System.out.println("Price =" + manga.getPrice() * book + "Bath");
            System.out.println("Please give me in 2 days");
        } else if (type == 2) {
            System.out.print("Please enter your book : ");
            int book = sc.nextInt();

            System.out.println("Price =" + magazine.getPrice() * book + "Bath");
            System.out.println("Please give me in 2 days");
        } else if (type == 3) {
            System.out.print("Please enter your book : ");
            int book = sc.nextInt();

            System.out.println("Price =" + programming.getPrice() * book + "Bath");
            System.out.println("Please give me in 2 days");
        }

    }

}
