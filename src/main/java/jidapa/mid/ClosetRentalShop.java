package jidapa.mid;

public class ClosetRentalShop {
    private String name;
    private int price;

    private int pieces;

    public ClosetRentalShop(String name, int price, int pieces) {
        this.name = name;
        this.price = price;

    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getPieces() {
        return pieces;
    }

    public void print() {
        System.out.println();
    }
}
